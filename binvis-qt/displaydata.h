/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef DISPLAYDATA_H
#define DISPLAYDATA_H
#define ShaderVariable GLuint
#include <GL/gl.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <mutex>
#endif
struct point{
    float x,y,z;
    float count;
};

struct displaydata{
    struct{
        GLuint shader_program;
        GLuint vertex_shader, fragment_shader;
        struct{
            ShaderVariable position, count, rotation, min_count;
        }variables;
        struct{
            char* vertex_shader, *fragment_shader;
        }path;
    }shaders;
    struct{
        struct point* points;
        GLuint vao, vbo;
#ifdef _WIN32
        CRITICAL_SECTION point_mutex[256];
#else
        std::mutex point_mutex[256];
#endif
    }vertices;
    struct{
        float rotation_x, rotation_y;
        int min_count;
        float alpha;
        float steps;
        float pos_scale;
    }translations;
};
#endif // DISPLAYDATA_H

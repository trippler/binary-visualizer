/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef PLOTTER_H
#define PLOTTER_H
#include <stdio.h>
#include "displaydata.h"
#include "libtask/task_manager.h"


#define LIST_INITIAL 0
#define LIST_ADD_TO_END 1
#define LIST_ADD_TO_START 2

#define SCALING_LINEAR 0
#define SCALING_LOGARITHMIC 1

//#define USE_MMAP

typedef enum{
    INITIAL,
    START,
    END
}add_location_t;

class Task_byte_plotter;

struct byte_plot_task_manager{
	Task_manager* manager;
	Task_byte_plotter* current_task, *next_task;
	struct data_container* data_cont;
};

struct filedatabuffer{
    long long position_in_file_buffer;
    long long position_in_alignment_table;
#ifdef USE_MMAP
    int filep;
#else
    FILE* filep;
#endif
    struct alignment_round* alignments;
    long long int alignment_max_value;
    int alignment_scaling;
    unsigned char* buffer;
    long long bytes_in_buffer;
    struct byte_plot_task_manager byte_plot;
};

struct plotter_settings{
    const char* filename;
    add_location_t location;
    int alignment;
    long int first_byte;
    long int last_byte;
    float add_value;
    displaydata* display_data;
    int scaling;
};
/*
 * This will only be allocated once and contain maximum resolution*pixeldepth for byte plot.
 * 1920*1200*4 is about 8.7M
 * Pulling 32M out of thin air as a good theoretical max resolution before it is dynamicly set
 */
#define DATA_CONTAINER_SIZE 32*1024*1024
struct data_container{
    unsigned char* data_ptr;
    int size;
    long long data_pos;
};

/*
 * Parses a file and maps each set of sequencial bytes {x,y,z} to the map in display_data
 */
long int plot_file(char*, displaydata*);
void add_buffer_to_displaydata(char* buffer, unsigned int count, displaydata* display_data);
void add_point_to_displaydata(struct displaydata* display_data, float add_value, int x, int y, int z);
void add_point_to_displaydata_lockfree(struct displaydata* display_data, float add_value, int x, int y, int z);
void remove_buffer_from_displaydata(char* buffer, unsigned int count, displaydata* display_data);
long int process_partfile_to_displaydata(const char* filename, displaydata* display_data, long int first_byte, long int last_byte, float add_value, add_location_t location);
long int process_partfile_to_displaydata_alignment(const char* filename, displaydata* display_data, long int first_byte, long int last_byte, float add_value, add_location_t location, int alignment, int scaling = SCALING_LINEAR, struct data_container* data_cont = NULL);
long int process_partfile_to_displaydata_settings(struct plotter_settings settings);
void add_partfile_to_displaydata(char* filename, displaydata* display_data, long int first_byte, long int last_byte, add_location_t location, int alignment = 8, int scaling = SCALING_LINEAR);
void remove_partfile_from_displaydata(char* filename, displaydata* display_data, long int first_byte, long int last_byte, add_location_t location, int alignment = 8, int scaling = SCALING_LINEAR);

struct alignment_round* get_alignment_table(int alignment);
long long get_alignment_table_max_value(int alignment);
long long get_next_value_from_filedatabuffer(struct filedatabuffer* data);
long long get_next_scaled_value_from_filedatabuffer(struct filedatabuffer* data);
bool no_more_data_in_filedatabuffer(struct filedatabuffer* data);
void add_datapoint_to_data_container(unsigned char val, struct data_container* data);
#ifdef USE_MMAP
struct filedatabuffer create_filedatabuffer(int file, struct alignment_round* alignment);
#else
struct filedatabuffer create_filedatabuffer(FILE* file, int alignment, int scaling, struct data_container* data_cont);
#endif
void destroy_filedatabuffer(struct filedatabuffer data);
#endif // PLOTTER_H

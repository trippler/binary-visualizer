/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "displaydata.h"
#include "displaydata_sorter.h"
#include <stdlib.h>
#include <QtGlobal>
#include <QDebug>
#include "error_messages.h"
int compare_display_element_points(const void* first, const void* second){
    struct point f = *((struct point *)first);
    struct point s = *((struct point *)second);
    if(f.count < s.count)return 1;
    else if(f.count == s.count)return 0;
    else if(f.count > s.count)return -1;
    else {
        ERROR("Should not get here!");
        return -2;
    }
}

void sort_displaydata_points(struct displaydata* display_data){
    qsort((void*)(display_data->vertices.points), (size_t)(256*256*256), sizeof(struct point), compare_display_element_points);
    for(int n = 1; n < 256*256*256; ++n)
        if(display_data->vertices.points[n-1].count < display_data->vertices.points[n].count)
            ERROR("Sorting flawed!");
    return;
}

int find_mincount_vertex_number(displaydata *display_data, int min_count)
{
    int max_count = 256*256*256;
    for(int n = 0; n < max_count; ++n){
        struct point* p = &(display_data->vertices.points[n]);
        if(p->count < min_count)
            return n-1;
    }
    return max_count;
}

struct point* private_sorter_vertexes;

int compare_display_element_indices(const void* first, const void* second){
    unsigned int f = *((unsigned int*)first);
    unsigned int s = *((unsigned int*)second);
    if(private_sorter_vertexes[f].count < private_sorter_vertexes[s].count)return 1;
    else if(private_sorter_vertexes[f].count == private_sorter_vertexes[s].count)return 0;
    else if(private_sorter_vertexes[f].count > private_sorter_vertexes[s].count)return -1;
    else {
        ERROR("Should not get here!");
        return -2;
    }
}

void sort_displaydata_indices(struct displaydata* display_data, size_t indice_count, unsigned int* indices){
    private_sorter_vertexes = display_data->vertices.points;
    qsort((void*)(&(indices[0])), indice_count, sizeof(unsigned int), compare_display_element_indices);
    return;
}

int find_mincount_index_number(displaydata *display_data, unsigned int* indices, int min_count, bool reversed)
{
	if(!reversed){
		int max_count = 256*256*256;
		for(int n = 0; n < max_count; ++n){
			struct point* p = &(display_data->vertices.points[indices[n]]);
			if(p->count < min_count)
				return n;
		}
		return max_count;
	}
	if(min_count <= 0)
		min_count = 1;
	int max_count = 256*256*256;
	for(int n = max_count-1; n > 0; --n){
		struct point* p = &(display_data->vertices.points[indices[n]]);
		if(p->count < min_count)
			return n;
	}
	return 0;
}

#ifndef HEX_VIEWER_WIDGET_H
#define HEX_VIEWER_WIDGET_H

#include <QWidget>
#include "QHexView.h"


namespace Ui {
class hex_viewer_widget;
}

class hex_viewer_widget : public QWidget
{
    Q_OBJECT

public:
    explicit hex_viewer_widget(QWidget *parent = 0);
    ~hex_viewer_widget();

private:
    Ui::hex_viewer_widget *ui;
    QHexView *hexview;


};

#endif // HEX_VIEWER_WIDGET_H

/*
Copyright (C) 2015  Niklas Trippler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <QCoreApplication>
#include "thread"
#include "chrono"

#include "task_manager.h"
#include "tasks/task_print.h"
#include "tasks/task_tpm.h"
#include "tasks/task_execute_every.h"

int main(int argc, char *argv[])
{
	Task_manager manager(128);
	Task* a = new Task_print("Task A (depends B)"), *b = new Task_print("Task B (depends C)"), *c = new Task_print("Task C"), *all = new Task_print("All (Depends on A,B and C)");
	a->add_dependency(b);
	a->set_delete_on_complete(true);
	b->add_dependency(c);
	b->set_delete_on_complete(true);
	all->add_dependency(a);
	all->add_dependency(b);
	all->add_dependency(c);
	all->set_delete_on_complete(true);
	c->set_delete_on_complete(true);
	manager.add_task(all);
	manager.add_task(a);
	manager.add_task(b);
	manager.add_task(c);

	Task_tpm tpm;
	manager.add_task(&tpm);

	Task_print spam("");

	Task_execute_every tee(std::chrono::milliseconds(100), &spam);
	manager.add_task(&tee);


	Task* comp[16];
	int count = 0;
	for(int n = 0; n < count; ++n){
		//		comp[n] = new Task_computation("Computation");
		manager.add_task(comp[n]);
	}
	while(manager.is_running()){
		std::this_thread::sleep_for(std::chrono::seconds(100));
		manager.stop();
	}
}

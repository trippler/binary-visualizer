/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "alignment_table_tests.h"
#include "plotter.h"
#include "error_messages.h"
#include <string.h>

bool alignment_table_generic_test(int alignment_table_to_test){
    //int alignment_table_to_test = 2;
    struct filedatabuffer fdb = create_filedatabuffer(NULL, alignment_table_to_test, 0, NULL);
    fdb.buffer[0] = 0b10101010;
    fdb.buffer[1] = 0b10101010;
    fdb.buffer[2] = 0b10101010;
    fdb.buffer[3] = 0b10101010;
    fdb.buffer[4] = 0b10101010;
    fdb.buffer[5] = 0b10101010;
    fdb.buffer[6] = 0b10101010;
    fdb.buffer[7] = 0b10101010;
    fdb.bytes_in_buffer = 8;
    int value = 0, prev_value = 0;
    for(int n = 0; n < (8*8)/alignment_table_to_test; ++n){
        prev_value = value;
        value = get_next_value_from_filedatabuffer(&fdb);
        //printf("n=%d,value=%d\r\n", n, value);
        if(value < 0 || value > get_alignment_table_max_value(alignment_table_to_test)){
            ERROR("Failed bounds test");
            return false;
        }
        if(prev_value != 0 && alignment_table_to_test % 2 == 0 && value != prev_value){
            ERROR("Failed equality test");
            return false;
        }
        if(prev_value != 0 && alignment_table_to_test % 2 == 1 && value != ((~prev_value) & get_alignment_table_max_value(alignment_table_to_test))){
            ERROR("Failed tilde test");
            return false;
        }
    }

    memset(fdb.buffer, 0, 32);
    fdb.position_in_alignment_table = 0;
    fdb.position_in_file_buffer = 0;
    for(int n = 0; n < (8*8)/alignment_table_to_test; ++n){
        if(get_next_value_from_filedatabuffer(&fdb) != 0){
            ERROR("Failed zero test");
            return false;
        }
    }

    memset(fdb.buffer, 0xFF, 32);
    fdb.position_in_alignment_table = 0;
    fdb.position_in_file_buffer = 0;
    for(int n = 0; n < (8*8)/alignment_table_to_test; ++n){
        if(get_next_value_from_filedatabuffer(&fdb) != get_alignment_table_max_value(alignment_table_to_test)){
            ERROR("Failed max-value test");
            return false;
        }
    }
    return true;
}

bool alignment_table_tests()
{
    INFO("Running alignment table tests");
    for(int n = 1; n < 9; ++n){
        INFO("Testing %d-bit alignment table", n);
        if(!alignment_table_generic_test(n)){
            ERROR("Alignment table test failed for %d-bit table", n);
            continue;
        }
    }
    INFO("Tests completed.");
    return true;
}

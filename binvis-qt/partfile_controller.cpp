/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "partfile_controller.h"
#include "error_messages.h"
#include "plotter.h"

partfile_controller::partfile_controller()
{
    start_offset = -1, stop_offset = -1;
    filename = 0;
    current_bit_alignment = 8;
    current_point_scaling_mode = SCALING_LINEAR;
    data.data_pos = 0;
    data.size = DATA_CONTAINER_SIZE;
    data.data_ptr = (unsigned char*)malloc(DATA_CONTAINER_SIZE);
}

partfile_controller::~partfile_controller()
{
    if(filename)
        free(filename);
    free(data.data_ptr);
}

void partfile_controller::set_filename(const char *filename)
{
    if(this->filename){
        free(this->filename);
        this->filename = 0;
    }
    if(filename){
        this->filename = (char*)malloc(strlen(filename)+1);
        strncpy(this->filename, filename, strlen(filename));
        this->filename[strlen(filename)] = 0;
    }
}

long long bytes_needing_read(long int old_start, long int new_start, long int old_stop, long int new_stop){
	long long bytes_to_read = 0;
	if(old_start < new_start)
		bytes_to_read += new_start - old_start;
	else
		bytes_to_read += old_start - new_start;
	if(old_stop < new_stop)
		bytes_to_read += new_stop - old_stop;
	else
		bytes_to_read += old_stop - new_stop;
	return bytes_to_read;
}

bool partfile_controller::set_buffer_offsets(long int new_start, long int new_stop, displaydata *display_data)
{
    if(new_stop <= new_start){
        ERROR("New stop is before new start");
        return false;
    }
    //If offsets are identical, don't do anything
    if(new_start == start_offset && new_stop == stop_offset)
        return false;
    //Completely different subset
    if(new_start > stop_offset || new_stop < start_offset || new_stop - new_start < bytes_needing_read(start_offset, new_start, stop_offset, new_stop)){
        //Clear all points
        clear_data(display_data);

        //Add new buffer
        INFO("Adding %ld to %ld", new_start, new_stop);
        add_partfile_to_displaydata(filename, display_data, new_start, new_stop, INITIAL, current_bit_alignment, current_point_scaling_mode);
        this->start_offset = new_start;
        this->stop_offset = new_stop;
        return true;
    }

    if(new_start < start_offset){
        INFO("Adding %ld to %ld", new_start, start_offset);
        add_partfile_to_displaydata(filename, display_data, new_start, start_offset, START, current_bit_alignment, current_point_scaling_mode);
    }
    if(new_start > start_offset){
        INFO("Removing %ld to %ld", start_offset, new_start);
        remove_partfile_from_displaydata(filename, display_data, start_offset, new_start, START, current_bit_alignment, current_point_scaling_mode);
    }
    if(new_stop < stop_offset){
        INFO("Removing %ld to %ld", new_stop, stop_offset);
        remove_partfile_from_displaydata(filename, display_data, new_stop+1, stop_offset, END, current_bit_alignment, current_point_scaling_mode);
    }
    if(new_stop > stop_offset){
        INFO("Adding %ld to %ld", stop_offset, new_stop);
        add_partfile_to_displaydata(filename, display_data, stop_offset+1, new_stop, END, current_bit_alignment, current_point_scaling_mode);
    }

    this->start_offset = new_start;
    this->stop_offset = new_stop;
    return true;
}

void partfile_controller::set_currently_active_buffer_offsets(long start, long stop)
{
    start_offset = start;
    stop_offset = stop;
}

void partfile_controller::set_bit_alignment(int alignment, displaydata* display_data)
{
    INFO("Setting new bit-alignment to: %d", alignment);
    if(current_bit_alignment == alignment)
        return;
    int start = start_offset;
    int stop = stop_offset;
    clear_data(display_data);
    current_bit_alignment = alignment;
    set_buffer_offsets(start, stop, display_data);
}

void partfile_controller::set_point_scaling(int scaling, displaydata* display_data)
{
    if(current_point_scaling_mode == scaling)
        return;
    int start = start_offset;
    int stop = stop_offset;
    clear_data(display_data);
    current_point_scaling_mode = scaling;
    set_buffer_offsets(start, stop, display_data);
}

long long partfile_controller::set_initial_file(const char *filename, displaydata* display_data)
{
    set_filename(filename);
    start_offset = 0;
    data.data_pos = 0;
    stop_offset = process_partfile_to_displaydata_alignment(filename, display_data, 0, LONG_MAX-1, 1.0f, INITIAL, current_bit_alignment, current_point_scaling_mode, &data);
    return stop_offset;
}

void partfile_controller::clear_data(displaydata* display_data)
{
    INFO("Resetting points");
    for(int n = 0; n < 256*256*256; ++n)
        display_data->vertices.points[n].count = 0.0f;
    start_offset = -1;
    stop_offset = -1;
}

/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "alignment_tables.h"
/*
struct alignment_round{
    unsigned char read_mask;
    char shift_mask;
    bool next_byte_after;
    bool value_finished;
    bool restart_alignment;
};
*/
struct alignment_round alignment_ptr_1bit[] = {
{0b10000000,    -7, false,  true,   false},
{0b01000000,    -6, false,  true,   false},
{0b00100000,    -5, false,  true,   false},
{0b00010000,    -4, false,  true,   false},
{0b00001000,    -3, false,  true,   false},
{0b00000100,    -2, false,  true,   false},
{0b00000010,    -1, false,  true,   false},
{0b00000001,    0, true,  true,   true}
};

struct alignment_round alignment_ptr_2bit[] = {
{0b11000000,    -6, false,  true,   false},
{0b00110000,    -4, false,  true,   false},
{0b00001100,    -2, false,  true,   false},
{0b00000011,    0, true,  true,   false},
};

struct alignment_round alignment_ptr_3bit[] = {
    {0b11100000,    -5, false,  true,   false},
    {0b00011100,    -2, false,  true,   false},
    {0b00000011,    1,  true,   false,  false},
    {0b10000000,    -7, false,  true,   false},
    {0b01110000,    -4, false,  true,   false},
    {0b00001110,    -1, false,  true,   false},
    {0b00000001,    2,  true,   false,  false},
    {0b11000000,    -6, false,  true,   false},
    {0b00111000,    -3, false,  true,   false},
    {0b00000111,    0,  true,   true,   true}
};

struct alignment_round alignment_ptr_4bit[] = {
    {0b11110000,    -4, false,  true,   false},
    {0b00001111,    0,  true,   true,   true}
};

struct alignment_round alignment_ptr_5bit[] = {
{0b11111000,    -3, false,  true,   false},
{0b00000111,    2, true,  false,   false},
{0b11000000,    -6, false,  true,   false},
{0b00111110,    -1, false,  true,   false},
{0b00000001,    4, true,  false,   false},
{0b11110000,    -4, false,  true,   false},
{0b00001111,    1, true,  false,   false},
{0b10000000,    -7, false,  true,   false},
{0b01111100,    -2, false,  true,   false},
{0b00000011,    3, true,  false,   false},
{0b11100000,    -5, false,  true,   false},
{0b00011111,    0, true,  true,   true}
};

struct alignment_round alignment_ptr_6bit[] = {
{0b11111100,    -2, false,  true,   false},
{0b00000011,    4, true,  false,   false},
{0b11110000,    -4, false,  true,   false},
{0b00001111,    2, true,  false,   false},
{0b11000000,    -6, false,  true,   false},
{0b00111111,    0, true,  true,   true}
};

struct alignment_round alignment_ptr_7bit[] = {
{0b11111110,    -1, false,  true,   false},
{0b00000001,    6, true,  false,   false},
{0b11111100,    -2, false, true,   false},
{0b00000011,    5, true,  false,   false},
{0b11111000,    -3, false, true,   false},
{0b00000111,    4, true,  false,   false},
{0b11110000,    -4, false, true,   false},
{0b00001111,    3, true,  false,   false},
{0b11100000,    -5, false, true,   false},
{0b00011111,    2, true,  false,   false},
{0b11000000,    -6, false, true,   false},
{0b00111111,    1, true,  false,   false},
{0b10000000,    -7, false, true,   false},
{0b01111111,    0, true,  true,   true},
};

struct alignment_round alignment_ptr_8bit[] = {
    {0b11111111,    0, true,  true,   true}
};

struct alignment_round alignment_ptr_16bit[] = {
    {0b11111111,    8, true,  false,   false},
    {0b11111111,    0, true,  true,   true}
};

struct alignment_round alignment_ptr_24bit[] = {
    {0b11111111,    16, true,  false,   false},
    {0b11111111,    8, true,  false,   false},
    {0b11111111,    0, true,  true,   true}
};

struct alignment_round alignment_ptr_32bit[] = {
    {0b11111111,    24, true,  false,   false},
    {0b11111111,    16, true,  false,   false},
    {0b11111111,    8, true,  false,   false},
    {0b11111111,    0, true,  true,   true}
};

struct alignment_round alignment_ptr_48bit[] = {
    {0b11111111,    40, true,  false,   false},
    {0b11111111,    32, true,  false,   false},
    {0b11111111,    24, true,  false,   false},
    {0b11111111,    16, true,  false,   false},
    {0b11111111,    8, true,  false,   false},
    {0b11111111,    0, true,  true,   true}
};

/*
Copyright (C) 2014  Niklas Trippler and Øyvind Andreas Eide

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
//bufferex.h
/*
	Header of the bufferex class by Hellbinder aka Trippler
	You may use or change this library in any way, but may not change this comment.
	If you use this library it would be nice if you give credit to me.*/
#ifndef bufferex_h
#define bufferex_h
#include <stdint.h>
class bufferElement{
    public:
    char *buffer;
	int64_t size;
	bufferElement *next;
	bufferElement(int64_t);
	~bufferElement();
};
class bufferex{
    public:
    int64_t size;
	bufferElement *first;
	bufferElement *last;
    void add(int64_t, const char *);
	bufferex();
	~bufferex();
	char *pszMainBuffer;
	char *genBuffer();
	void clearBuffers();
	void optimize();
};
//#include "bufferex.cpp"
#endif 
